using System.Threading.Tasks;
using Otus.Blog.Domain;

namespace Otus.Blog.Application
{
  public interface IUserService
  {
    Task<User> AddNewUser(User user);
  }
}
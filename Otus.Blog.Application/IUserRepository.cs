using System.Threading.Tasks;
using Otus.Blog.Domain;

namespace Otus.Blog.Application
{
  public interface IUserRepository
  {
    Task<User> GetById(int id);
    Task<User> GetByLogin(string login);
    Task<User> Add(User user);
  }
}
﻿using Microsoft.EntityFrameworkCore;
using Otus.Blog.Domain;

namespace Otus.Blog.Infrastructure
{
  public class BlogDbContext : DbContext
  {
    public BlogDbContext(DbContextOptions options) : base(options)
    {
    }

    public DbSet<User> Users { get; set; }
    public DbSet<Category> Categories { get; set; }
    public DbSet<Article> Articles { get; set; }
    public DbSet<Comment> Comments { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder
        .Entity<Article>()
        .Property(x => x.Tags).HasColumnType("varchar(100)[]");

      modelBuilder
        .Entity<Article>()
        .Property(x => x.DocumentMetadata)
        .HasColumnType("jsonb");
    }
  }
}
﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Blog.Application;
using Otus.Blog.Domain;
using StackExchange.Redis;

namespace Otus.Blog.Infrastructure
{
  public class UserRepository : IUserRepository
  {
    private readonly BlogDbContext _db;
    private readonly ILogger _logger;
    private readonly IDatabase _redis;
    private readonly ISubscriber _subscriber;

    public UserRepository(BlogDbContext db, IDatabase redis, ISubscriber subscriber, ILogger<User> logger)
    {
      _db = db;
      _redis = redis;
      _subscriber = subscriber;
      _logger = logger;
    }

    public async Task<User> GetById(int id)
    {
      var fromCache = await GetUserFromCache(id);
      if (fromCache != null)
      {
        return fromCache;
      }

      var user = await _db.Users.FirstOrDefaultAsync(x => x.Id == id);

      // Операция асинхронная, но в данном случае нам не важен результат,
      // поэтому через _ = можно явно это указать 
      _ = AddUserToCache(user);

      return user;
    }

    public Task<User> GetByLogin(string login)
    {
      return _db.Users.FirstOrDefaultAsync(x => x.Login == login);
    }

    public async Task<User> Add(User user)
    {
      _db.Users.Add(user);

      // По-хорошему сохранение должно быть на уровень выше в Unit-of-Work
      // кому интересно, можете посмотреть лекцию тут
      // https://vk.com/video-173214385_456239057
      await _db.SaveChangesAsync();

      // можем кинуть сообщение в редис, что юзер был создан,
      // чтобы другие сервисы (если они у нас есть/будут) могли выполнить какую-то свою логику,
      // например какой-нибудь MailService мог бы отправить приветственное письмо пользователю
      _ = _subscriber.PublishAsync("events:user:added", JsonSerializer.Serialize(user), CommandFlags.FireAndForget);

      return user;
    }

    private async Task<User> GetUserFromCache(int id)
    {
      var redisKey = GetCacheKey(id);
      string cache = await _redis.StringGetAsync(redisKey);

      if (!string.IsNullOrEmpty(cache))
      {
        try
        {
          return JsonSerializer.Deserialize<User>(cache);
        }
        catch (Exception e)
        {
          _logger.LogError(e, "Could not deserialize user");
        }
      }

      return null;
    }

    private async Task AddUserToCache(User user)
    {
      if (user != null)
      {
        var redisKey = GetCacheKey(user.Id);
        await _redis.StringSetAsync(redisKey, JsonSerializer.Serialize(user), flags: CommandFlags.FireAndForget);
      }
    }

    private static string GetCacheKey(int id)
    {
      return $"users:id_{id}";
    }
  }
}
using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Blog.Application;
using Otus.Blog.Domain;

namespace Otus.Blog.WebApp.Controllers
{
  [ApiController]
  [Route("[controller]")]
  public class UserController : ControllerBase
  {
    private readonly IUserRepository _userRepository;
    private readonly IUserService _userService;

    public UserController(IUserRepository userRepository, IUserService userService)
    {
      _userRepository = userRepository;
      _userService = userService;
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<User>> Get(int id)
    {
      var user = await _userRepository.GetById(id);
      if (user == null)
      {
        return NotFound();
      }

      return Ok(user);
    }

    [HttpPost]
    public async Task<ActionResult<User>> Get([FromBody] User user)
    {
      if (user == null)
      {
        throw new ArgumentNullException(nameof(user));
      }

      await _userService.AddNewUser(user);

      return Ok(user);
    }
  }
}
using System.Collections.Generic;

namespace Otus.Blog.Domain
{
  public class Category
  {
    public int Id { get; set; }
    public string Name { get; set; }
    public IEnumerable<Article> Articles { get; set; }
  }
}
using System;

namespace Otus.Blog.Domain
{
  public class DocumentMetadata
  {
    public string UserId { get; set; }
    public DateTime? Created { get; set; }
    public DateTime? Updated { get; set; }
    public DateTime? Deleted { get; set; }
  }
}
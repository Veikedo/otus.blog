using System.Collections.Generic;

namespace Otus.Blog.Domain
{
  public class Article
  {
    public int Id { get; set; }
    public string Title { get; set; }
    public string Body { get; set; }
    public User Author { get; set; }
    public Category Category { get; set; }
    public IEnumerable<Comment> Comments { get; set; }
    public List<string> Tags { get; set; }
    public List<DocumentMetadata> DocumentMetadata { get; set; }
  }
}